import requests

t_url = 'https://ww.example.com/api/credits/stock/'
custom_headers = {'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:68.0) Gecko/20100101 Firefox/68.0'}



def fetch_req():
    print(f"Fetch req for\t{t_url} ")
    res_data = requests.get(t_url,headers=custom_headers)
    res_headers = res_data.headers
    print("\nResponse Headers\n")
    for k,v in res_headers.items():
        print(f'{k}:{v}')
    print("\n\n")
    print(f'\t{res_data.status_code}\t{res_headers["Connection"]}')
    # print(dir(res_data))
    print("\n",res_data.cookies.get_dict())
    print(res_data.text)





if __name__ == '__main__':
    print("\n\n*************THE Requester****************\n\n")
    fetch_req()
