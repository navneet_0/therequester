import requests
import re
import json
import csv
import sys
from custom_headers import custom_headers


class TheRequester:
    http_suffix = "https://"
    regex_http = re.compile(r'^http')
    regex_links = urls = re.compile('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+')

    def set_target(self):
        target_host = input("Target host e.g(hackerone.com)\t:\t")
        if target_host:
            return self.http_suffix+target_host
        else:
            print("Invalid input")
            self.set_target()

    def set_method(self):
        method = 'get'
        req_method = input("ENTER\g: GET\tp: POST\th: HEAD\to: OPTIONS\t:")
        if req_method:
            if req_method == 'g':
                method = 'get'
            elif req_method == 'p':
                method = 'post'
            elif req_method == 'o':
                method = 'options'
            else:
                print("Invalid input")
                self.set_method()
            return method
        else:
            print("Default GET:")
            return method

    def run_requester(self,targ,meth):
        print("The Requester:")
        req_str = "requests."+meth+'('+targ+')'
        print(req_str)
        # res = req_obj
        res = requests.get(targ)
        output_file = meth+targ[8:]+'.txt'
        print(output_file)
        # output_file = 'output.txt'
        try:
            with open(output_file, 'w') as data:
                data.writelines(res.text)
            return output_file
        except:
            print("Error Writing data to "+output_file)

    def extract_links(self,input_data):
        links_page = []
        print("Extract links from a page")
        if input_data:
            with open(input_data,'r') as data_input:
                r = data_input.read()
                # urls = re.findall('https?://(?:[-\w.]|(?:%[\da-fA-F]{2}))+', r)
                urls = re.findall(self.regex_links,r)
                print(urls)
                # if self.regex_http.search(r):
                #     print(r)
                # print(r)
                print("\n")

            print("Finished")

    def get_cookies(self,target):
        target = 'https://www.superdrug.com/'
        print(f"Fetching cookies from {target}")
        res_data = requests.get(target, headers=custom_headers)
        res_headers = res_data.headers
        print("\nResponse Headers\n")
        print("\n\n")
        print(f'\t{res_data.status_code}\t{res_headers["Connection"]}')
        # print(dir(res_data))
        print("\n", res_data.cookies.get_dict())
        # print(res_data.text)





if __name__ == '__main__':
    t = ''
    print(custom_headers)
    req_obj = TheRequester()
    # t = req_obj.set_target()
    # m = req_obj.set_method()
    req_obj.get_cookies(t)
    # # print(t,m)
    # # req_obj.run_requester(t,m)
    # req_obj.extract_links(req_obj.run_requester(t,m))
